console.log("Hello World!");

/*
	IF Statement
		- executes a statement if a specified condition is TRUE
		- can stand alone without the ELSE statement
*/

/*
	SYNTAX:
		if(condition){
			code block;
		}
*/

/*function takeNumber(num){
	return prompt("Input Number:");
}

let numA = takeNumber();*/
/*
if(numA < 0){
	console.log("NumA is LESS THAN ZERO");
}
else{
	console.log("Number is GREATER THAN OR EQUAL TO ZERO");
}*/


let city = "New York";

if(city === "New York"){
	console.log("Welcome to New York City!");
}

// ELSE IF statement
	/*
		- used with IF statement
		- conditions to be verify when IF condtions return FALSE
	*/

let numA = -1;
let numB = 1;

if(numA > 0){
	console.log("Hello");
}
else if(numB > 0){
	console.log("World");
}

city = "Tokyo";

if(city === "New York"){
	console.log("Welcome to New York City!");
}
else if(city === "Tokyo"){
	console.log("Welcom to Tokyo!");
}

// ELSE Statement	
	/*
		executes the code block if all IFs and ELSE IFs return as FALSE
	*/


// Another example
/*
let age = prompt("Please enter your age: ");

if (age <= 18){
	console.log("Not allowed to drink!");
} 
else{
	console.log("Allowed to drink!");
}

*/
/*
	MINIACTIVITY
		create a conditional statement if the height is below 150cm, display "Did not pass the minimum height requirement"
		if 150 and above, "Passed the minimum height requirement."

*/

/*function heightCheck(height){
	console.log("Your height is " + height);

	if(height < 150){
		return "P";
	}
	else{
		return "F";
	}

}

let userHeight = heightCheck(prompt("Input your height: "))

if(userHeight = "P"){
		console.log("Did not pass the minimum height requirement.")
	}
	else{
		console.log("Passed the minimum height requirement.")
	}*/


let height = 160;

if (height < 150){
	console.log("Did not pass the minimum height requirement.")
}
else {
	console.log("Passed the minimum height requirements.")
}


function heightCheck(height){
	/*
	let userHeight = prompt("Input Height: ")
	console.log("Your height is " + userHeight);
	if (userHeight < 150){
		console.log("Did not pass the minimum height requirement.")
	}
	else {
		console.log("Passed the minimum height requirements.")
	}
	*/
	console.log("Your height is " + height);
	if (height < 150){
		console.log("Did not pass the minimum height requirement.")
	}
	else {
		console.log("Passed the minimum height requirements.")
	}

}

heightCheck(150);
heightCheck(160);
heightCheck(300);


function determineTyphoonIntensity(windSpeed){
	if (windSpeed < 30){
		return 'Not a typhoon yet.'
	}
	else if (windSpeed <= 61){
		return 'Tropical Depression detected';
	}
	else if (windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical Storm detected';
	}
	else if (windSpeed >= 89 && windSpeed <= 117){
		return 'Severe Tropical Storm detected';
	}
	else {
		return 'Typhoon detected';
	}
}

let message = determineTyphoonIntensity(69)

console.log(message);


if (message === "Tropical Storm detected"){
	console.warn(message);
}


// TRUTHY and FALSY values
/*
	- in JS, a TRUTHY value is a value that is considered TRUE when encountered in a Boolean context.

	- Values are considered TRUE unless defined otherwise 
	- FALSY values/exception for TRUTHY
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN (Not a Number)
*/

// TRUTHY Examples
if(true){
	console.log("Truthy!");
}

if(1){
	console.log("Truthy!");
}

if([]){
	console.log("Truthy!");
}


// FALSY Examples
if(undefined){
	console.log("Falsy!");
}
if(NaN){
	console.log("Falsy!");
}
if(0){
	console.log("Falsy!");
}
if(-0){
	console.log("Falsy!");
}
if(null){
	console.log("Falsy!");
}


// Conditional (TERNARY) Operators
	/*
		The Ternary operator takes in three operands
			1. condition
			2. expression to execute if the condition is Truthy
			3. expression to execute if the condition is Falsy
	
		SYNTAX
			(condition) ? ifTrue : ifFalse
	*/


// Single statement	 execution
let ternaryResult = (1 < 18) ? "Statement is True" : "Statement is False";
console.log("The result of Ternary Operator is: \n" + ternaryResult);

let name;

function isOfLegalAge() {
	name = "John";
	return	"You are of the legal age limit";
}

function isUnderAge() {
	name = "Jane";
	return	"You are under the age limit";
}
/*
let age = parseInt(prompt("What is your age?"));
	// parseInt function - returns int value instead of default string.

console.log(age);
console.log(typeof age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ', ' + name);
*/

// SWITCH Statement
	/*
		SYNTAX
			switch (expression) {
				case value :
					statement;
					break;
				default:
					statement;
					break;
			}
	*/

let day = prompt("What day of the week is it today?").toLowerCase();
	// .toLowercase() - function to translate acquire input to lowercase.

console.log(day);

	switch(day){
		case 'monday' :
			console.log("The color of the day is red");
			break;
		case 'tuesday' :
			console.log("The color of the day is orange");
			break;
		case 'wednesday' :
			console.log("The color of the day is yellow");
			break;
		case 'thursday' :
			console.log("The color of the day is green");
			break;	
		case 'friday' :
			console.log("The color of the day is blue");
			break;		
		case 'saturday' :
			console.log("The color of the day is indigo");
			break;	
		case 'sunday' :
			console.log("The color of the day is violet");
			break;		
		/*
		case ('sunday'||'saturday') :
			console.log("The color of the day is violet");
			break;	
		*/

		default:
			console.log("Please input a valid day");
			break;
	}


// Try-Catch-Finally Statement
	// used when handling errors.

function showIntensityAlert(windSpeed){
	try {
		// attempt to execute a code
		alerat(determineTyphoonIntensity(windSpeed))
	}
	catch (error){
		// will catch errors within the "try" code block
		console.log(error)
		console.warn(error.message)
	}
		// continue the execution of the code regardless of try and catch code execution.
	finally{
		alert("Intensity update will show new alert.")
	}
}

showIntensityAlert(56);